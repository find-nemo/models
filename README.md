Create a file name `package.json` and copy the content below. After copying run `npm install` to download the dependencies

```
{
  "name": "models",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+ssh://git@gitlab.com/find-nemo/models.git"
  },
  "author": "",
  "license": "ISC",
  "bugs": {
    "url": "https://gitlab.com/find-nemo/models/issues"
  },
  "homepage": "https://gitlab.com/find-nemo/models#readme",
  "description": "",
  "dependencies": {
    "class-transformer": "^0.4.0",
    "class-validator": "^0.13.1",
    "sequelize-typescript": "^2.1.0"
  }
}
```
