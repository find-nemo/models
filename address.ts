import { Type } from 'class-transformer';
import {
  IsDefined,
  IsInt,
  IsLatitude,
  IsLongitude,
  IsOptional,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  Column,
  CreatedAt,
  DataType,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

import { User } from './user';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'address',
})
export class Address extends Model<Address> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsDefined({ groups: ['updateAddress'] })
  @IsInt({ groups: ['getById', 'deleteById'] })
  @Min(0, { groups: ['getById', 'deleteById'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['getById', 'deleteById'] })
  @Type(() => Number)
  id!: number;

  @AllowNull(false)
  @Column({
    type: DataType.STRING,
    field: 'address_1',
  })
  @IsDefined({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsString({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MinLength(1, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MaxLength(255, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  address1!: string;

  @Column({
    type: DataType.STRING,
    field: 'address_2',
  })
  @IsOptional({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsString({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MinLength(1, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MaxLength(255, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  address2?: string;

  @Column({
    type: DataType.STRING,
    field: 'address_3',
  })
  @IsOptional({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsString({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MinLength(1, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MaxLength(255, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  address3?: string;

  @AllowNull(false)
  @Column
  @IsDefined({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsString({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MinLength(1, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MaxLength(255, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  city!: string;

  @AllowNull(false)
  @Column
  @IsDefined({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsString({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MinLength(1, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MaxLength(255, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  state!: string;

  @AllowNull(false)
  @Column
  @IsDefined({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsString({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MinLength(1, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MaxLength(255, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  country!: string;

  @AllowNull(false)
  @Column
  @IsDefined({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsString({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MinLength(1, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @MaxLength(255, {
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  postalCode!: string;

  @Column(DataType.DOUBLE)
  @IsDefined({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsLatitude({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @Type(() => Number)
  latitude!: number;

  @Column(DataType.DOUBLE)
  @IsDefined({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @IsLongitude({
    groups: ['createDriver', 'updateDriver', 'createAddress', 'updateAddress'],
  })
  @Type(() => Number)
  longitude!: number;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @HasMany(() => User)
  users!: User[];
}
