import { Type } from 'class-transformer';
import {
  IsDate,
  IsEnum,
  IsInt,
  IsLatitude,
  IsLongitude,
  IsOptional,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

import { Device } from './device';
import { Group } from './group';
import { User } from './user';

enum TripType {
  FROMWARDS = 'FROMWARDS',
  TOWARDS = 'TOWARDS',
}

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'trip',
})
export class Trip extends Model<Trip> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsInt({ groups: ['end', 'update_location'] })
  @Min(0, { groups: ['end', 'update_location'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['end', 'update_location'] })
  @Type(() => Number)
  id!: number;

  @AllowNull(false)
  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  @IsInt()
  @Min(1)
  @Max(Number.MAX_SAFE_INTEGER)
  @Type(() => Number)
  driverId!: number;

  @AllowNull(false)
  @ForeignKey(() => Group)
  @Column(DataType.BIGINT)
  @IsInt({
    groups: ['getTripsByGroupId', 'start'],
  })
  @Min(1, {
    groups: ['getTripsByGroupId', 'start'],
  })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['getTripsByGroupId', 'start'],
  })
  @Type(() => Number)
  groupId!: number;

  @AllowNull(false)
  @ForeignKey(() => Device)
  @Column(DataType.BIGINT)
  @IsInt({ groups: [] })
  @Min(1, { groups: [] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: [] })
  @Type(() => Number)
  deviceId!: number;

  @Column(DataType.DATE)
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  startTime?: Date | null;

  @Column(DataType.DATE)
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  endTime?: Date | null;

  @Column(DataType.DOUBLE)
  @IsOptional()
  @IsLatitude({ groups: ['start'] })
  @Type(() => Number)
  startLatitude?: number;

  @Column(DataType.DOUBLE)
  @IsOptional()
  @IsLongitude({ groups: ['start'] })
  @Type(() => Number)
  startLongitude?: number;

  @Column(DataType.DOUBLE)
  @IsOptional()
  @IsLatitude({ groups: ['end'] })
  @Type(() => Number)
  endLatitude?: number;

  @Column(DataType.DOUBLE)
  @IsOptional()
  @IsLongitude({ groups: ['end'] })
  @Type(() => Number)
  endLongitude?: number;

  @AllowNull(false)
  @Default(TripType.TOWARDS)
  @Column
  @IsOptional({ groups: ['start'] })
  @IsEnum(TripType, { groups: ['start'] })
  @MinLength(1, { groups: ['start'] })
  @MaxLength(255, { groups: ['start'] })
  type!: TripType;

  @Column(DataType.DOUBLE)
  @IsOptional({ groups: [] })
  @IsLatitude({ groups: ['update_location'] })
  @Type(() => Number)
  driverCurrentLatitude?: number;

  @Column(DataType.DOUBLE)
  @IsOptional({ groups: [] })
  @IsLongitude({ groups: ['update_location'] })
  @Type(() => Number)
  driverCurrentLongitude?: number;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => User)
  @ValidateNested()
  @Type(() => User)
  driver!: User;

  @BelongsTo(() => Group)
  @ValidateNested()
  @Type(() => Group)
  group!: Group;

  @BelongsTo(() => Device)
  @ValidateNested()
  @Type(() => Device)
  device!: Device;

  @IsOptional()
  @IsString({ groups: ['start'] })
  @MinLength(1, { groups: ['start'] })
  @MaxLength(255, { groups: ['start'] })
  uId?: string;

  childId?: number;
}
