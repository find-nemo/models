import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsInt,
  IsMilitaryTime,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  Unique,
  UpdatedAt,
} from 'sequelize-typescript';

import { School } from './school';
import { User } from './user';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'group',
})
export class Group extends Model<Group> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsInt({ groups: ['updateGroup', 'deleteGroupById'] })
  @Min(0, { groups: ['updateGroup', 'deleteGroupById'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['updateGroup', 'deleteGroupById'] })
  @Type(() => Number)
  id!: number;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  @IsNumber({})
  @Min(1)
  @Max(Number.MAX_SAFE_INTEGER)
  creatorId!: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  @IsOptional({ groups: ['updateGroup'] })
  @IsString({ groups: ['createGroup', 'updateGroup'] })
  @MinLength(1, { groups: ['createGroup', 'updateGroup'] })
  @MaxLength(255, { groups: ['createGroup', 'updateGroup'] })
  name!: string;

  @ForeignKey(() => School)
  @Column(DataType.BIGINT)
  @IsOptional({ groups: ['updateGroup'] })
  @IsNumber({}, { groups: ['createGroup', 'updateGroup'] })
  @Min(1, { groups: ['createGroup', 'updateGroup'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['createGroup', 'updateGroup'] })
  schoolId!: number;

  @Default(false)
  @Column(DataType.BOOLEAN)
  @IsBoolean()
  isDeleted!: boolean;

  @Column(DataType.STRING)
  @IsOptional({ groups: ['createGroup', 'updateGroup'] })
  @IsString({ groups: ['createGroup', 'updateGroup'] })
  @IsMilitaryTime({ groups: ['createGroup', 'updateGroup'] })
  fromTime?: string;

  @Column(DataType.STRING)
  @IsOptional({ groups: ['createGroup', 'updateGroup'] })
  @IsString({ groups: ['createGroup', 'updateGroup'] })
  @IsMilitaryTime({ groups: ['createGroup', 'updateGroup'] })
  toTime?: string;

  @AllowNull(false)
  @PrimaryKey
  @Unique
  @Column(DataType.STRING)
  @MinLength(1, { groups: ['getByPublicId'] })
  @MaxLength(10, { groups: ['getByPublicId'] })
  @IsString({ groups: ['getByPublicId'] })
  publicId!: string;

  @Column({ type: DataType.INTEGER })
  @IsOptional({ groups: ['createGroup', 'updateGroup'] })
  @Max(99999, { groups: ['createGroup', 'updateGroup'] })
  @IsInt({ groups: ['createGroup', 'updateGroup'] })
  @Type(() => Number)
  monthFees?: number;

  @Column({ type: DataType.INTEGER })
  @IsOptional({ groups: ['createGroup', 'updateGroup'] })
  @Max(99999, { groups: ['createGroup', 'updateGroup'] })
  @IsInt({ groups: ['createGroup', 'updateGroup'] })
  @Type(() => Number)
  threeMonthsFees?: number;

  @Column({ type: DataType.INTEGER })
  @IsOptional({ groups: ['createGroup', 'updateGroup'] })
  @Max(99999, { groups: ['createGroup', 'updateGroup'] })
  @IsInt({ groups: ['createGroup', 'updateGroup'] })
  @Type(() => Number)
  sixMonthsFees?: number;

  @Column({ type: DataType.INTEGER })
  @IsOptional({ groups: ['createGroup', 'updateGroup'] })
  @Max(99999, { groups: ['createGroup', 'updateGroup'] })
  @IsInt({ groups: ['createGroup', 'updateGroup'] })
  @Type(() => Number)
  twelveMonthsFees?: number;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => User)
  creator!: User;

  @BelongsTo(() => School)
  school!: School;

  @IsOptional({ groups: ['createGroup', 'updateGroup'] })
  @IsBoolean({ groups: ['createGroup', 'updateGroup'] })
  @Type(() => Boolean)
  updateChildFees?: boolean;
}
