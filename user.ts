import { Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsDefined,
  IsEmail,
  IsEnum,
  IsInt,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import moment from 'moment';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  Unique,
  UpdatedAt,
} from 'sequelize-typescript';

import { Address } from './address';
import { UserType } from './user-type';
import { UserTypeEnum } from './user-type.enum';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'user',
})
export class User extends Model<User> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @Min(0, { groups: ['getById'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['getById'] })
  @IsNumber({}, { groups: ['getById'] })
  id!: number;

  @AllowNull(false)
  @Column
  @IsDefined({ groups: ['create', 'update', 'addParentChildToGroup'] })
  @IsString({ groups: ['create', 'update', 'addParentChildToGroup'] })
  @MinLength(1, { groups: ['create', 'update', 'addParentChildToGroup'] })
  @MaxLength(255, { groups: ['create', 'update', 'addParentChildToGroup'] })
  fullName!: string;

  @AllowNull(false)
  @Unique
  @Column
  @IsDefined({
    groups: ['update', 'addParentChildToGroup', 'getGroupByPhoneNumber'],
  })
  @IsPhoneNumber('IN', {
    groups: ['update', 'addParentChildToGroup', 'getGroupByPhoneNumber'],
  })
  phoneNumber!: string;

  @Column
  @IsOptional({ groups: ['create', 'update'] })
  @IsString({ groups: ['create', 'update'] })
  @MinLength(1, { groups: ['create', 'update'] })
  @MaxLength(255, { groups: ['create', 'update'] })
  profileImageUrl?: string;

  @Column
  @IsOptional({ groups: ['create', 'update'] })
  @IsString({ groups: ['create', 'update'] })
  @MinLength(1, { groups: ['create', 'update'] })
  @MaxLength(255, { groups: ['create', 'update'] })
  photoIdUrl?: string;

  @ForeignKey(() => Address)
  @Column(DataType.BIGINT)
  @IsOptional({ groups: ['create', 'update'] })
  @IsNumber({}, { groups: ['create', 'update'] })
  @Min(1, { groups: ['create', 'update'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['create', 'update'] })
  addressId?: number;

  @Column
  @IsOptional({ groups: ['create', 'update'] })
  @IsString({ groups: ['create', 'update'] })
  @IsEmail({}, { groups: ['create', 'update'] })
  @MinLength(1, { groups: ['create', 'update'] })
  @MaxLength(255, { groups: ['create', 'update'] })
  email?: string;

  @Column
  @IsOptional({ groups: ['create', 'update'] })
  @Transform((params) => moment(params.value).format('YYYY-MM-DD'))
  @IsDate({ groups: ['create', 'update'] })
  dateOfBirth?: Date;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => Address)
  @IsOptional({ groups: ['create', 'update'] })
  @ValidateNested({ groups: ['create', 'update'] })
  @Type(() => Address)
  address?: Address;

  @HasMany(() => UserType)
  userTypes!: UserType[];

  @IsEnum(UserTypeEnum, {
    groups: ['create', 'add-role'],
  })
  role?: UserTypeEnum;

  @IsArray({ groups: ['getByUserIds'] })
  @IsInt({ groups: ['getByUserIds'], each: true })
  @Min(0, { groups: ['getByUserIds'], each: true })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['getByUserIds'],
    each: true,
  })
  @Type(() => Number)
  userIds!: number[];
}
