import { Type } from 'class-transformer';
import {
  IsInt,
  IsNumber,
  IsString,
  Max,
  Min,
  MinLength,
  MaxLength,
  IsOptional,
  IsBoolean,
} from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import { Child } from './child';

import { User } from './user';
import { PaymentStatusEnum } from './payment-status-enum';

@Table({
  timestamps: false,
  underscored: true,
  tableName: 'payment',
})
export class Payment extends Model<Payment> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsInt({ groups: ['confirmDriverTransaction', 'confirmParentTransaction'] })
  @Min(0, { groups: ['confirmDriverTransaction', 'confirmParentTransaction'] })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['confirmDriverTransaction', 'confirmParentTransaction'],
  })
  @Type(() => Number)
  id!: number;

  @AllowNull(false)
  @Column({
    type: DataType.STRING,
    field: 'uId',
  })
  @MinLength(1, { groups: ['update', 'delete'] })
  @MaxLength(255, { groups: ['update', 'delete'] })
  @IsString({ groups: ['update', 'delete'] })
  @Type(() => String)
  uId!: string;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  @IsNumber({}, { groups: ['create'] })
  @Min(1, { groups: ['create'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['create'] })
  @Type(() => Number)
  driverId!: number;

  @ForeignKey(() => Child)
  @Column(DataType.BIGINT)
  @IsNumber({}, { groups: ['create'] })
  @Min(1, { groups: ['create'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['create'] })
  @Type(() => Number)
  childId!: number;

  @Column({ type: DataType.DECIMAL(13, 4) })
  @IsOptional({ groups: ['update'] })
  @Max(999999999, { groups: ['create', 'update'] })
  @IsNumber({ maxDecimalPlaces: 4 }, { groups: ['create', 'update'] })
  @Type(() => Number)
  amount!: number;

  @Default(false)
  @Column(DataType.BOOLEAN)
  @IsBoolean()
  @Type(() => Boolean)
  hasDriverAccepted!: boolean;

  @Default(false)
  @Column(DataType.BOOLEAN)
  @IsBoolean()
  @Type(() => Boolean)
  hasParentAccepted!: boolean;

  @AllowNull(false)
  @Column(DataType.STRING)
  @IsOptional({ groups: ['update'] })
  @MinLength(1, { groups: ['create', 'update'] })
  @MaxLength(255, { groups: ['create', 'update'] })
  @IsString({ groups: ['create', 'update'] })
  @Type(() => String)
  dates!: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  status!: PaymentStatusEnum;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  @IsNumber({})
  @Min(1)
  @Max(Number.MAX_SAFE_INTEGER)
  @Type(() => Number)
  creatorId!: number;

  @Column(DataType.DATE)
  createdAt!: Date;

  @Column(DataType.TEXT)
  @MinLength(0, { groups: ['create', 'update'] })
  @MaxLength(60000, { groups: ['create', 'update'] })
  @IsOptional({ groups: ['create', 'update'] })
  @IsString({ groups: ['create', 'update'] })
  @Type(() => String)
  notes!: string;

  @BelongsTo(() => User)
  driver!: User;

  @BelongsTo(() => User)
  creator!: User;

  @BelongsTo(() => Child)
  child!: Child;

  @IsOptional({ groups: ['getByDriverId', 'getByChildId'] })
  @IsInt({ groups: ['getByDriverId', 'getByChildId'] })
  @Type(() => Number)
  limit?: number;

  @IsOptional({ groups: ['getByDriverId', 'getByChildId'] })
  @IsInt({ groups: ['getByDriverId', 'getByChildId'] })
  @Type(() => Number)
  offset?: number;
}
