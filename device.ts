import { Type } from 'class-transformer';
import {
  IsNumber,
  IsOptional,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

import { User } from './user';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'device',
})
export class Device extends Model<Device> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsNumber({}, { groups: ['deleteDeviceById', 'deleteDeviceTokenById'] })
  @Min(0, { groups: ['deleteDeviceById', 'deleteDeviceTokenById'] })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['deleteDeviceById', 'deleteDeviceTokenById'],
  })
  @Type(() => Number)
  id!: number;

  @AllowNull(false)
  @Column({
    type: DataType.STRING,
    field: 'uId',
  })
  @IsString({ groups: ['getByUIdAndPhoneNumber', 'createDevice'] })
  @MinLength(1, { groups: ['getByUIdAndPhoneNumber', 'createDevice'] })
  @MaxLength(255, { groups: ['getByUIdAndPhoneNumber', 'createDevice'] })
  uId!: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  @IsOptional({ groups: ['createDevice'] })
  @IsString({ groups: ['createDevice'] })
  @MinLength(1, { groups: ['createDevice'] })
  @MaxLength(255, { groups: ['createDevice'] })
  model?: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  @IsOptional({ groups: ['createDevice'] })
  @IsString({ groups: ['createDevice'] })
  @MinLength(1, { groups: ['createDevice'] })
  @MaxLength(255, { groups: ['createDevice'] })
  make?: string;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  @IsNumber({}, { groups: [] })
  @Min(1, { groups: [] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: [] })
  userId!: number;

  @Column(DataType.TEXT)
  @IsOptional({ groups: ['createDevice'] })
  @IsString({ groups: ['createDevice'] })
  @MinLength(1, { groups: ['createDevice'] })
  fcmToken?: string | null;

  @Column(DataType.TEXT)
  @IsOptional({ groups: ['createDevice'] })
  @IsString({ groups: ['createDevice'] })
  @MinLength(1, { groups: ['createDevice'] })
  locale?: string;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => User)
  @IsOptional()
  @ValidateNested()
  @Type(() => User)
  user!: User;
}
