import { Type } from 'class-transformer';
import {
  IsDefined,
  IsEnum,
  IsInt,
  IsPhoneNumber,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import {
  AllowNull,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

import { Group } from './group';
import { Trip } from './trip';
import { User } from './user';

export enum DriverGroupPermissionEnum {
  READ = 'READ',
  WRITE = 'WRITE',
  OWNER = 'OWNER',
}

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'driver_group',
})
export class DriverGroup extends Model<DriverGroup> {
  @AllowNull(false)
  @ForeignKey(() => User)
  @PrimaryKey
  @Column(DataType.BIGINT)
  @IsDefined({ groups: ['deleteDriverFromGroup'] })
  @IsInt({ groups: ['deleteDriverFromGroup'] })
  @Min(0, { groups: ['deleteDriverFromGroup'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['deleteDriverFromGroup'] })
  @Type(() => Number)
  driverId!: number;

  @AllowNull(false)
  @ForeignKey(() => Group)
  @PrimaryKey
  @Column(DataType.BIGINT)
  @IsDefined({
    groups: [
      'addDriverToGroup',
      'deleteDriverFromGroup',
      'getDriversByGroupId',
    ],
  })
  @IsInt({
    groups: [
      'addDriverToGroup',
      'deleteDriverFromGroup',
      'getDriversByGroupId',
    ],
  })
  @Min(0, {
    groups: [
      'addDriverToGroup',
      'deleteDriverFromGroup',
      'getDriversByGroupId',
    ],
  })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: [
      'addDriverToGroup',
      'deleteDriverFromGroup',
      'getDriversByGroupId',
    ],
  })
  @Type(() => Number)
  groupId!: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  @IsEnum(DriverGroupPermissionEnum)
  permission!: DriverGroupPermissionEnum;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => User)
  driver!: User;

  @BelongsTo(() => Group)
  group!: Group;

  latestTrip?: Trip;

  @IsDefined({ groups: ['addDriverToGroup'] })
  @IsString({ groups: ['addDriverToGroup'] })
  @MinLength(1, { groups: ['addDriverToGroup'] })
  @MaxLength(255, { groups: ['addDriverToGroup'] })
  driverFullName?: string;

  @IsDefined({
    groups: ['addDriverToGroup'],
  })
  @IsPhoneNumber('IN', {
    groups: ['addDriverToGroup'],
  })
  driverPhoneNumber?: string;
}
