import { Type } from 'class-transformer';
import { IsDate, IsInt, IsLatitude, IsLongitude, IsOptional, Max, Min, ValidateNested } from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

import { Child } from './child';
import { Trip } from './trip';
import { User } from './user';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'pickup_dropoff',
})
export class PickupDropoff extends Model<PickupDropoff> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsInt()
  @Min(0)
  @Max(Number.MAX_SAFE_INTEGER)
  @Type(() => Number)
  id!: number;

  @AllowNull(false)
  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  @IsInt({ groups: [] })
  @Min(1, { groups: [] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: [] })
  @Type(() => Number)
  driverId!: number;

  @AllowNull(false)
  @ForeignKey(() => Trip)
  @Column(DataType.BIGINT)
  @IsInt({
    groups: ['getPickupDropoffByTripId', 'pickupDropoffChild', 'absentChild'],
  })
  @Min(1, {
    groups: ['getPickupDropoffByTripId', 'pickupDropoffChild', 'absentChild'],
  })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['getPickupDropoffByTripId', 'pickupDropoffChild', 'absentChild'],
  })
  @Type(() => Number)
  tripId!: number;

  @AllowNull(false)
  @ForeignKey(() => Child)
  @Column(DataType.BIGINT)
  @IsInt({ groups: [] })
  @Min(1, { groups: [] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: [] })
  @Type(() => Number)
  childId!: number;

  @Column(DataType.DATE)
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  pickupTime?: Date | null;

  @Column(DataType.DOUBLE)
  @IsOptional()
  @IsLatitude({ groups: ['pickupChild'] })
  @Type(() => Number)
  pickupLatitude?: number;

  @Column(DataType.DOUBLE)
  @IsOptional()
  @IsLongitude({ groups: ['pickupChild'] })
  @Type(() => Number)
  pickupLongitude?: number | null;

  @Column(DataType.DOUBLE)
  @IsOptional()
  @IsLatitude({ groups: ['dropoffChild'] })
  @Type(() => Number)
  dropoffLatitude?: number | null;

  @Column(DataType.DOUBLE)
  @IsOptional()
  @IsLongitude({ groups: ['dropoffChild'] })
  @Type(() => Number)
  dropoffLongitude?: number | null;

  @Column(DataType.DATE)
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  dropoffTime?: Date | null;

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  @Type(() => Boolean)
  isAbsent!: boolean;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => User)
  @IsOptional()
  @ValidateNested()
  @Type(() => User)
  driver!: User;

  @BelongsTo(() => Child)
  @IsOptional()
  @ValidateNested()
  @Type(() => Child)
  child!: Child;

  @BelongsTo(() => Trip)
  @ValidateNested()
  @Type(() => Trip)
  trip!: Trip;

  @IsInt({ groups: ['pickupChild', 'dropoffChild', 'absentChild'], each: true })
  @Min(1, {
    groups: ['pickupChild', 'dropoffChild', 'absentChild'],
    each: true,
  })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['pickupChild', 'dropoffChild', 'absentChild'],
    each: true,
  })
  @Type(() => Number)
  childIds!: number[];
}
