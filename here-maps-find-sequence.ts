/* eslint-disable @typescript-eslint/no-explicit-any */
export interface Waypoint {
  id: string;
  lat: number;
  lng: number;
  sequence: number;
  estimatedArrival?: any;
  estimatedDeparture?: any;
  fulfilledConstraints: any[];
}

export interface Interconnection {
  fromWaypoint: string;
  toWaypoint: string;
  distance: number;
  time: number;
  rest: number;
  waiting: number;
}

export interface TimeBreakdown {
  driving: number;
  service: number;
  rest: number;
  waiting: number;
}

export interface Result {
  waypoints: Waypoint[];
  distance: string;
  time: string;
  interconnections: Interconnection[];
  description: string;
  timeBreakdown: TimeBreakdown;
}

export interface HereMapsFindSequenceResponse {
  results: Result[];
  errors: any[];
  processingTimeDesc: string;
  responseCode: string;
  warnings?: any;
  requestId?: any;
  timestamp?: number;
}
