import { Type } from 'class-transformer';
import {
  IsDefined,
  IsInt,
  IsOptional,
  IsString,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Index,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { Address } from './address';
import { User } from './user';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'school',
})
export class School extends Model<School> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsInt({ groups: ['searchById'] })
  @Min(0, { groups: ['searchById'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['searchById'] })
  @Type(() => Number)
  id!: number;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  @IsInt()
  @Min(1)
  @Max(Number.MAX_SAFE_INTEGER)
  @Type(() => Number)
  createdBy?: number | null;

  @AllowNull(false)
  @ForeignKey(() => Address)
  @Column(DataType.BIGINT)
  @IsInt({ groups: ['create'] })
  @Min(1, { groups: ['create'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['create'] })
  @Type(() => Number)
  addressId!: number;

  @Index('idx_school_fullname')
  @Column(DataType.STRING)
  @IsDefined({ groups: ['searchByName', 'create'] })
  @IsString({ groups: ['searchByName', 'create'] })
  @Type(() => String)
  fullName!: string;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => User)
  @ValidateNested()
  @Type(() => User)
  creater!: User;

  @BelongsTo(() => Address)
  @ValidateNested()
  @Type(() => Address)
  address!: Address;

  @IsOptional({ groups: ['searchByName', 'searchById', 'getDriverSchools'] })
  @IsString({ groups: ['searchByName', 'searchById', 'getDriverSchools'] })
  @Type(() => String)
  includeAddress?: string;

  @IsOptional({ groups: ['searchByName', 'searchById', 'getDriverSchools'] })
  @IsString({ groups: ['searchByName', 'searchById', 'getDriverSchools'] })
  @Type(() => String)
  includeCreator?: string;
}
