import { Type } from 'class-transformer';
import {
  IsDefined,
  IsNumber,
  IsOptional,
  IsPhoneNumber,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

import { Address } from './address';
import { User } from './user';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'child',
})
export class Child extends Model<Child> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsNumber({}, { groups: ['updateChild', 'deleteChildById'] })
  @Min(0, { groups: ['updateChild', 'deleteChildById'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['updateChild', 'deleteChildById'] })
  @Type(() => Number)
  id!: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  @IsString({ groups: ['createChild', 'updateChild', 'addParentChildToGroup'] })
  @MinLength(1, {
    groups: ['createChild', 'updateChild', 'addParentChildToGroup'],
  })
  @MaxLength(255, {
    groups: ['createChild', 'updateChild', 'addParentChildToGroup'],
  })
  fullName!: string;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  @IsNumber({}, { groups: [] })
  @Min(1, { groups: [] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: [] })
  parentId!: number;

  @ForeignKey(() => Address)
  @Column(DataType.BIGINT)
  @IsOptional({ groups: ['createChild', 'updateChild'] })
  @IsNumber({}, { groups: ['createChild', 'updateChild'] })
  @Min(1, { groups: ['createChild', 'updateChild'] })
  @Max(Number.MAX_SAFE_INTEGER, { groups: ['createChild', 'updateChild'] })
  addressId?: number;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => User)
  @IsOptional()
  @ValidateNested({ groups: ['addParentChildToGroup'], each: true })
  @Type(() => User)
  parent!: User;

  @BelongsTo(() => Address)
  @IsOptional()
  @ValidateNested()
  @Type(() => Address)
  address?: Address;

  @IsDefined({ groups: ['addParentChildToGroup'] })
  @IsString({ groups: ['addParentChildToGroup'] })
  @MinLength(1, { groups: ['addParentChildToGroup'] })
  @MaxLength(255, { groups: ['addParentChildToGroup'] })
  parentFullName?: string;

  @Column
  @IsOptional()
  @IsString()
  @MinLength(1)
  @MaxLength(255)
  profileImageUrl?: string;

  @IsDefined({
    groups: ['addParentChildToGroup'],
  })
  @IsPhoneNumber('IN', {
    groups: ['addParentChildToGroup'],
  })
  parentPhoneNumber?: string;
}
