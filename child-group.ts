import { Type } from 'class-transformer';
import { IsArray, IsInt, IsOptional, Max, Min } from 'class-validator';
import {
  AllowNull,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

import { Child } from './child';
import { Group } from './group';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'child_group',
})
export class ChildGroup extends Model<ChildGroup> {
  @AllowNull(false)
  @ForeignKey(() => Child)
  @PrimaryKey
  @Column(DataType.BIGINT)
  @IsInt({ groups: ['getChildsByChildId', 'updateChildGroupMonthlyFees'] })
  @Min(0, { groups: ['getChildsByChildId', 'updateChildGroupMonthlyFees'] })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['getChildsByChildId', 'updateChildGroupMonthlyFees'],
  })
  @Type(() => Number)
  childId!: number;

  @AllowNull(false)
  @ForeignKey(() => Group)
  @PrimaryKey
  @Column(DataType.BIGINT)
  @IsInt({ groups: ['getChildsByGroupId', 'updateChildGroupMonthlyFees'] })
  @Min(0, { groups: ['getChildsByGroupId', 'updateChildGroupMonthlyFees'] })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['getChildsByGroupId', 'updateChildGroupMonthlyFees'],
  })
  @Type(() => Number)
  groupId!: number;

  @Column({ type: DataType.INTEGER })
  @IsOptional({ groups: ['updateChildGroupMonthlyFees'] })
  @Max(99999, { groups: ['updateChildGroupMonthlyFees'] })
  @IsInt({ groups: ['updateChildGroupMonthlyFees'] })
  @Type(() => Number)
  monthFees?: number;

  @Column({ type: DataType.INTEGER })
  @IsOptional({ groups: ['updateChildGroupMonthlyFees'] })
  @Max(99999, { groups: ['updateChildGroupMonthlyFees'] })
  @IsInt({ groups: ['updateChildGroupMonthlyFees'] })
  @Type(() => Number)
  threeMonthsFees?: number;

  @Column({ type: DataType.INTEGER })
  @IsOptional({ groups: ['updateChildGroupMonthlyFees'] })
  @Max(99999, { groups: ['updateChildGroupMonthlyFees'] })
  @IsInt({ groups: ['updateChildGroupMonthlyFees'] })
  @Type(() => Number)
  sixMonthsFees?: number;

  @Column({ type: DataType.INTEGER })
  @IsOptional({ groups: ['updateChildGroupMonthlyFees'] })
  @Max(99999, { groups: ['updateChildGroupMonthlyFees'] })
  @IsInt({ groups: ['updateChildGroupMonthlyFees'] })
  @Type(() => Number)
  twelveMonthsFees?: number;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => Child)
  child!: Child;

  @BelongsTo(() => Group)
  group!: Group;

  @IsArray({ groups: ['addChildToGroup', 'removeChildFromGroup'] })
  @IsInt({ groups: ['addChildToGroup', 'removeChildFromGroup'], each: true })
  @Min(0, { groups: ['addChildToGroup', 'removeChildFromGroup'], each: true })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['addChildToGroup', 'removeChildFromGroup'],
    each: true,
  })
  @Type(() => Number)
  childIds!: number[];
}
