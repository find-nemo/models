import { registerDecorator, ValidationOptions } from 'class-validator';
import { Payment } from './payment';

export function IsTruthy(validationOptions?: ValidationOptions) {
  return (object: Record<string, unknown>, propertyName: string): void => {
    registerDecorator({
      name: 'isTruthy',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: unknown): boolean {
          return typeof value === 'string' && value === 'true'; // you can return a Promise<boolean> here as well, if you want to make async validation
        },
      },
    });
  };
}

export function IsFeesDatesValid(validationOptions?: ValidationOptions) {
  const re = /^((1[0-2]|[1-9])-(3[01]|[12][0-9]|[1-9]),)*((1[0-2]|[1-9])-(3[01]|[12][0-9]|[1-9])){1}$/gi;
  return (object: Payment, propertyName: string): void => {
    registerDecorator({
      name: 'isFeesDatesValid',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: string): boolean {
          return typeof value === 'string' && re.test(value); // you can return a Promise<boolean> here as well, if you want to make async validation
        },
      },
    });
  };
}
