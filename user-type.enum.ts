export enum UserTypeEnum {
  DRIVER = 'driver',
  PARENT = 'parent',
  BOTH = 'both',
}
