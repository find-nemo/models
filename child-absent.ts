import { Type } from 'class-transformer';
import { IsDateString, IsInt, Max, Min } from 'class-validator';
import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

import { Child } from './child';

@Table({
  timestamps: true,
  underscored: true,
  tableName: 'child_absent',
})
export class ChildAbsent extends Model<ChildAbsent> {
  @AllowNull(false)
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  @IsInt({
    groups: ['update', 'deleteById'],
  })
  @Min(0, {
    groups: ['update', 'deleteById'],
  })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['update', 'deleteById'],
  })
  @Type(() => Number)
  id!: number;

  @AllowNull(false)
  @ForeignKey(() => Child)
  @PrimaryKey
  @Column(DataType.BIGINT)
  @IsInt({ groups: ['create', 'update', 'getUpcomingDates', 'getPastDates'] })
  @Min(0, { groups: ['create', 'update', 'getUpcomingDates', 'getPastDates'] })
  @Max(Number.MAX_SAFE_INTEGER, {
    groups: ['create', 'update', 'getUpcomingDates', 'getPastDates'],
  })
  @Type(() => Number)
  childId!: number;

  @AllowNull(false)
  @PrimaryKey
  @Column(DataType.DATEONLY)
  @IsDateString(
    {},
    { groups: ['create', 'update', 'updateChildGroupMonthlyFees'] }
  )
  @Type(() => Date)
  from!: Date;

  @AllowNull(false)
  @PrimaryKey
  @Column(DataType.DATEONLY)
  @IsDateString(
    {},
    { groups: ['create', 'update', 'updateChildGroupMonthlyFees'] }
  )
  @Type(() => Date)
  to!: Date;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;

  @BelongsTo(() => Child)
  child!: Child;
}
