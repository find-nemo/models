export interface Interconnection {
  fromWaypoint: string;
  toWaypoint: string;
  distance: number;
  time: number;
}

export interface FindSequenceResponse {
  interconnections: Interconnection[];
  waypoints: string[];
  timestamp?: number;
}

export interface FindSequenceByTrip {
  tripId: number;
  sequence: FindSequenceResponse;
}
