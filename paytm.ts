import { Type } from 'class-transformer';
import { IsString, MinLength, MaxLength, IsDecimal } from 'class-validator';

export class PaytmInitiateTransactionBody {
  @MinLength(1, { groups: ['initiateTransaction'] })
  @MaxLength(255, { groups: ['initiateTransaction'] })
  @IsString({ groups: ['initiateTransaction'] })
  @Type(() => String)
  orderId!: string;

  @MinLength(0, { groups: ['initiateTransaction'] })
  @MaxLength(60000, { groups: ['initiateTransaction'] })
  @IsDecimal({ decimal_digits: '2' }, { groups: ['initiateTransaction'] })
  @Type(() => String)
  amount!: string;
}

export interface PaytmSecret {
  mid: string;
  mkey: string;
  websiteName: string;
  hostname: string;
}
